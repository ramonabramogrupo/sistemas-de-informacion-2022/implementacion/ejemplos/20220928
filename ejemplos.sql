﻿USE empresas;

/**
 Consulta 1
  Muestro las personas que su dato1 es mayor que 5. De 
  estas quiero ver el nombre, los apellidos y su dato1
**/
SELECT 
    p.nombre,
    p.apellidos,
    p.dato1
  FROM 
    personas p
  WHERE 
    p.dato1>5;

/**
 CONSULTA 2
  Mostrar todas las personas con un campo nuevo 
  denominado mesFecha que representa el mes de la 
  fecha de nacimiento
 **/

SELECT 
    p.*,
    MONTH(p.fecha) AS mesFecha
  FROM 
    personas p;


/**
  Consulta 3
  Mostrar las personas que hayan nacido en septiembre

 **/

  -- realizada con funciones
  SELECT 
        p.* 
      FROM 
        personas p
      WHERE 
        MONTH(p.fecha)=9;

  -- realizada con comodines (LIKE) 
    SELECT 
        p.* 
      FROM 
        personas p
      WHERE
        p.fecha LIKE "%-09-%";
  
/** 
  CONSULTA 4
  Mostrar el nombre y apellidos 
  de las personas cuya poblacion comience por L

  **/

  -- realizada con funciones (LEFT)
  SELECT DISTINCT 
      p.nombre,p.apellidos
    FROM 
      personas p
    WHERE 
      LEFT(p.poblacion,1)='L';
           
  -- realizada con comodines
SELECT DISTINCT
    p.nombre,p.apellidos
  FROM 
    personas p
  WHERE
    p.poblacion LIKE 'L%';


/**
 Consulta 5
  Listar el nombre y los apellidos de las personas
  que el segundo caracter de su poblacion sea una 'o'

 **/

  -- utilizando comodines (_%)

  SELECT DISTINCT
      nombre,p.apellidos
    FROM 
      personas p
    WHERE 
      p.poblacion LIKE '_o%';


  -- utilizando funciones (LEFT y RIGHT)
  -- LEFT(campo,caracteres)
  -- RIGHT(campo,caracteres)

  SELECT DISTINCT
      nombre,p.apellidos,p.poblacion
    FROM 
      personas p
    WHERE 
      RIGHT(LEFT(p.poblacion,2),1)='o';
  
  -- utilizando funciones (SUBSTRING)
  -- SUBTRING(campo,posicion,[caracteres])

  SELECT DISTINCT
      nombre,p.apellidos,p.poblacion
    FROM 
      personas p
    WHERE 
      SUBSTRING(p.poblacion,2,1)='o';
