﻿SET NAMES 'utf8';
DROP DATABASE IF EXISTS practica8Ejercicio2; 
CREATE DATABASE practica8Ejercicio2;
USE practica8Ejercicio2; 

CREATE TABLE camionero(
  dni varchar(10),
  nombre varchar(10),
  poblacion varchar(100),
  tfno varchar(10),
  direccion varchar(500),
  salario float,
  PRIMARY KEY(dni)
  );

CREATE TABLE camion(
  matricula varchar(7),
  modelo varchar(100),
  potencia int,
  tipo varchar(100),
  PRIMARY KEY(matricula)
  );

CREATE TABLE conduce(
  dniCamionero varchar(10),
  matCamion varchar(7),
  PRIMARY KEY(dniCamionero,matCamion)
  );

CREATE TABLE paquete(
  codigo int AUTO_INCREMENT,
  descripcion varchar(500),
  destinatario varchar(200),
  direccion varchar(500),
  codProvincia int,
  PRIMARY KEY(codigo)
  );

CREATE TABLE distribuye(
  dniCamionero varchar(10),
  codPaquete int,
  PRIMARY KEY(dniCamionero,codPaquete),
  UNIQUE KEY(codPaquete)
  );

CREATE TABLE provincia(
  codigo int AUTO_INCREMENT,
  nombre varchar(200),
  PRIMARY KEY(codigo)
  );

ALTER TABLE conduce
  ADD CONSTRAINT fkConduceCamionero
    FOREIGN KEY (dniCamionero)
    REFERENCES camionero(dni)
    ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT fkConduceCamion
    FOREIGN KEY (matCamion)
    REFERENCES camion(matricula)
    ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE paquete 
  ADD CONSTRAINT fkPaqueteProvincia
    FOREIGN KEY (codProvincia)
    REFERENCES provincia(codigo)
    ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE distribuye
  ADD CONSTRAINT fkDistribuyeCamionero
    FOREIGN KEY (dniCamionero)
    REFERENCES camionero(dni)
    ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT fkDistribuyePaquete
    FOREIGN KEY (codPaquete)
    REFERENCES paquete(codigo)
    ON DELETE RESTRICT ON UPDATE RESTRICT;