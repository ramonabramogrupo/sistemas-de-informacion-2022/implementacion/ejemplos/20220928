﻿SET NAMES 'utf8';
DROP DATABASE IF EXISTS practica8Ejercicio1; 
CREATE DATABASE practica8Ejercicio1;
USE practica8Ejercicio1; 


CREATE TABLE cliente(
  dni varchar(10),
  nombre varchar(100),
  apellidos varchar(200),
  fecha_nac date,
  tfno varchar(20),
  PRIMARY KEY(dni)
  );

CREATE TABLE compra(
  dniCliente varchar(10),
  codigoProducto int,
  PRIMARY KEY(dniCliente,codigoProducto)
  );

CREATE TABLE producto(
  codigo int AUTO_INCREMENT,
  nombre varchar(100),
  precio float,
  PRIMARY KEY(codigo)
  );

CREATE TABLE suministra(
  codigoProducto int,
  nifProveedor varchar(10),
  PRIMARY KEY(codigoProducto,nifProveedor),
  UNIQUE KEY (codigoProducto)
  );

CREATE TABLE proveedor(
  nif varchar(10),
  nombre varchar(100),
  direccion varchar(500),
  PRIMARY KEY(nif)
  );

ALTER TABLE compra 
  ADD CONSTRAINT fkCompraCliente 
    FOREIGN KEY (dniCliente)
    REFERENCES cliente(dni)
    ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT fkCompraProducto
    FOREIGN KEY (codigoProducto)
    REFERENCES producto(codigo)
    ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE suministra 
  ADD CONSTRAINT fkSuministraProducto
    FOREIGN KEY (codigoProducto)
    REFERENCES producto(codigo)
    ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT fkSuministraProveedor
    FOREIGN KEY (nifProveedor)
    REFERENCES proveedor(nif)
    ON DELETE RESTRICT ON UPDATE RESTRICT;